<?php

declare(strict_types=1);

namespace App\Domain\Exchange;

use JsonSerializable;

class Rate implements JsonSerializable
{

    /**
     * Numeric exchange code
     * @var int
     */
    private int $numCode;
    /**
     * String exchange code
     * @var string
     */
    private string $charCode;
    /**
     * Exchange nominal
     * @var int
     */
    private int $nominal;
    /**
     * Exchange name
     * @var string
     */
    private string $name;
    /**
     * Exchange rate
     * @var float
     */
    private float $value;

    /**
     * @return int
     */
    public function getNumCode(): int
    {
        return $this->numCode;
    }

    /**
     * @param int $numCode
     * @return self
     */
    public function setNumCode(int $numCode): self
    {
        $this->numCode = $numCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCharCode(): string
    {
        return $this->charCode;
    }

    /**
     * @param string $charCode
     * @return self
     */
    public function setCharCode(string $charCode): self
    {
        $this->charCode = $charCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getNominal(): int
    {
        return $this->nominal;
    }

    /**
     * @param int $nominal
     * @return self
     */
    public function setNominal(int $nominal): self
    {
        $this->nominal = $nominal;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return self
     */
    public function setValue(float $value): self
    {
        $this->value = $value;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'numCode' => $this->numCode,
            'charCode' => $this->charCode,
            'nominal' => $this->nominal,
            'name' => $this->name,
            'value' => $this->value,
        ];
    }
}