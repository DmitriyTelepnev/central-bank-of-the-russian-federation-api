<?php


namespace App\Domain\Exchange;

use DateTime;

/**
 * Rate repository interface
 * @package App\Domain\Exchange
 */
interface RateRepository
{

    /**
     * Get exchange rates for date
     * @param DateTime $dateTime
     * @return array
     */
    public function getByDate(DateTime $dateTime): array;

}