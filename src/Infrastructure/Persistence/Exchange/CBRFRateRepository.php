<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Exchange;

use App\Domain\Exchange\Rate;
use App\Domain\Exchange\RateRepository;
use DateTime;
use Exception;

/**
 * Implementation of RateRepository for Central Bank of the Russian Federation XML API
 * @package App\Infrastructure\Persistence\Exchange
 */
class CBRFRateRepository implements RateRepository
{

    const GET_EXCHANGE_RATES_URL = 'http://www.cbr.ru/scripts/XML_daily.asp';
    const GET_EXCHANGE_RATES_DATE_PARAM = 'date_req';

    /**
     * @inheritDoc
     * @param DateTime $dateTime
     * @return array
     * @throws Exception
     */
    public function getByDate(DateTime $dateTime): array
    {
        $xml = simplexml_load_string($this->sendRequest($dateTime));

        $exchangeRates = [];
        foreach ($xml->Valute as $rawExchange) {
            $exchangeRates[] = (new Rate())
                ->setValue(floatval(str_replace(',', '.', (string) $rawExchange->Value[0])))
                ->setNumCode((int) $rawExchange->NumCode[0])
                ->setCharCode((string) $rawExchange->CharCode[0])
                ->setNominal((int) $rawExchange->Nominal[0])
                ->setName((string) $rawExchange->Name[0]);
        }

        return $exchangeRates;
    }

    /**
     * @param DateTime $dateTime
     * @return string
     * @throws Exception
     */
    private function sendRequest(DateTime $dateTime): string
    {
        $uriParams = http_build_query([self::GET_EXCHANGE_RATES_DATE_PARAM => $dateTime->format('d/m/Y')]);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::GET_EXCHANGE_RATES_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $uriParams);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

        $data = curl_exec($ch);

        $curlErr = curl_error($ch);
        if ($curlErr) {
            throw new Exception($curlErr);
        }

        if (!is_string($data)) {
            throw new Exception('invalid response');
        }

        curl_close($ch);

        return $data;
    }
}