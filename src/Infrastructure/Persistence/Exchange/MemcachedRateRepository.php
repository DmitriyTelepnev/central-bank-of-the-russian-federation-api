<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Exchange;

use App\Domain\Exchange\RateRepository;
use DateTime;
use Memcached;

/**
 * Caching decorator for RateRepository, using Memcached
 * @package App\Infrastructure\Persistence\Exchange
 */
class MemcachedRateRepository implements RateRepository
{

    const EXCHANGE_RATE_KEY = 'exchange-rate:';

    /**
     * @var Memcached
     */
    private Memcached $memcached;

    /**
     * @var RateRepository
     */
    private RateRepository $rateRepository;

    public function __construct(Memcached $memcachedConn, RateRepository $rateRepository)
    {
        $this->memcached = $memcachedConn;
        $this->rateRepository = $rateRepository;
    }

    /**
     * @inheritDoc
     * @param DateTime $dateTime
     * @return array
     */
    public function getByDate(DateTime $dateTime): array
    {
        $rateKey = $this->getKey($dateTime);
        $rates = $this->memcached->get($rateKey);
        if (!$rates) {
            $rates = $this->rateRepository->getByDate($dateTime);
            $this->memcached->set($rateKey, $rates);
        }

        return $rates;
    }

    /**
     * Cache key for current date
     * @param DateTime $dateTime
     * @return string
     */
    private function getKey(DateTime $dateTime): string
    {
        return self::EXCHANGE_RATE_KEY . '' . $dateTime->format('d/m/Y');
    }
}