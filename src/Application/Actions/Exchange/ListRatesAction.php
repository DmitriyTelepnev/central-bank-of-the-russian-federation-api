<?php
declare(strict_types=1);

namespace App\Application\Actions\Exchange;

use App\Application\Actions\Action;
use App\Domain\Exchange\RateRepository;
use Psr\Http\Message\ResponseInterface as Response;
use DateTime;
use DateInterval;
use Psr\Log\LoggerInterface;

class ListRatesAction extends Action
{

    /**
     * @var RateRepository
     */
    private RateRepository $rateRepository;

    /**
     * ListRatesAction constructor.
     * @param RateRepository $rateRepository
     * @param LoggerInterface $logger
     */
    public function __construct(RateRepository $rateRepository, LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->rateRepository = $rateRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $yesterday = (new DateTime())->add(DateInterval::createFromDateString('yesterday'));

        $rates = $this->rateRepository->getByDate($yesterday);

        return $this->respondWithData($rates);
    }
}