# Central Bank of the Russian Federation API
see [conditions](CONDITIONS.md)

## Requirements

* docker
* docker-compose

## Run it

```bash
docker-compose up
```

## Test it

```
curl http://localhost:8080/rates
{
    "statusCode": 200,
    "data": [
        {
            "numCode": 36,
            "charCode": "AUD",
            "nominal": 1,
            "name": "\u0410\u0432\u0441\u0442\u0440\u0430\u043b\u0438\u0439\u0441\u043a\u0438\u0439 \u0434\u043e\u043b\u043b\u0430\u0440",
            "value": 55.1773
        },
        {
            "numCode": 944,
            "charCode": "AZN",
            "nominal": 1,
            "name": "\u0410\u0437\u0435\u0440\u0431\u0430\u0439\u0434\u0436\u0430\u043d\u0441\u043a\u0438\u0439 \u043c\u0430\u043d\u0430\u0442",
            "value": 43.7039
        },
......


```

## Improvements

* For increase velocity of request processing you may use `nginx + php-fpm` instead `php -S`.
* If you want not to depend on the availability of the Central Bank of the Russian Federation API, you must store the values of the rates at home, - any analytical database will do
* If CBRF exchange response will be large - you must read and parse response line by line