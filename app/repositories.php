<?php
declare(strict_types=1);

use App\Domain\Exchange\RateRepository;
use App\Infrastructure\Persistence\Exchange\MemcachedRateRepository;
use App\Infrastructure\Persistence\Exchange\CBRFRateRepository;
use Psr\Container\ContainerInterface;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        RateRepository::class => function(ContainerInterface $c): RateRepository {
            $memcachedConn = new Memcached();
            $memcachedConn->addServer($c->get('settings')['memcached']['host'], $c->get('settings')['memcached']['port']);

            return new MemcachedRateRepository($memcachedConn, new CBRFRateRepository());
        },
    ]);
};
